﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Note
    {
        private String noteText;
        private String noteAuthor;
        private int notePriority;
        public Note()
        {
            this.noteText = " ";
            this.noteAuthor = "Unknown Author";
            this.notePriority = 1;
        }
        public Note(string text, string author)
        {
            this.noteText = text;
            this.noteAuthor = author;
            this.notePriority = 5;
        }
        public Note(string text, string author, int priority)
        {
            this.noteText = text;
            this.noteAuthor = author;
            this.notePriority = priority;
        }
        public string getText()
        {
            return noteText;
        }
        public string getAuthor()
        {
            return noteAuthor;
        }
        public void setText(string text)
        {
            this.noteText = text;
        }
        public void setPriority(int priority)
        {
            this.notePriority = priority;
        }
        public string note_Text
        {
            get { return this.noteText; }
            set { this.noteText = value; }
        }
        public string note_Author
        {
            get { return this.noteAuthor; }
        }
        public int note_Priority
        {
            get { return this.notePriority; }
            set { this.notePriority = value; }
        }
        public override string ToString()
        {
            return this.noteText + ", " + this.noteAuthor;
        }
    }
}
