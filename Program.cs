﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote = new Note();
            Note secondNote = new Note("Budenje u 8h", "Tin");
            Note thirdNote = new Note("Napisati lv", "Tin", 2);

            Console.WriteLine(firstNote.getText());
            Console.WriteLine(firstNote.getAuthor());

            Console.WriteLine(secondNote.getText());
            Console.WriteLine(secondNote.getAuthor());

            Console.WriteLine(thirdNote.getText());
            Console.WriteLine(thirdNote.getAuthor());

            DateTime time = DateTime.Now;
            TimestampNote time_note = new TimestampNote("Napisati seminar", "Tin", 1, time);
            Console.WriteLine(time_note.ToString());
        }
    }
}
