﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class TimestampNote : Note
    {
        public DateTime time;
        public TimestampNote(string text, string author, int priority, DateTime current_time):
            base(text, author, priority)
        {
            this.time = current_time;
        }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return this.note_Text + ", " + this.note_Author + ", modified: " + this.time;
        }
    }
}
